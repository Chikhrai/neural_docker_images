import * as tf from "@tensorflow/tfjs-node";
import { imageSize } from "image-size";
// import { promisify } from 'util';
import { promises } from "fs";
import { join } from "path";

(async () => {
  const image = await promises.readFile(join(__dirname, "cat.jpg"));
  const { width, height } = imageSize(image);

  console.log(image, width, height);

  const tensor = tf.node.decodeJpeg(image, 3);
  const data = tensor.dataSync();
})();
