# Build new image

docker build -t deepdream-caffe -f Dockerfile-cuda-caffe .
docker build -t deepdream-anaconda -f Dockerfile-cuda-anaconda .

# Run

docker run -d --gpus all deepdream-caffe
docker run -d --gpus all -p 8888:8888 deepdream-anaconda